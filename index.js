const express = require("express")
const mongoose = require("mongoose")
const routes = require("./routes/routes")
const cors =  require("cors")


require("dotenv").config()
const mongoString = process.env.DATABASE_URL

mongoose.connect(mongoString)
const database = mongoose.connection

database.on("error", (error) => {
console.log(error)
})

database.once("connected", () => {
    console.log("Connected to DB")
})



const app = express()


app.use(express.json())
app.use(cors({
    origin: '*'
}));


app.use(express.urlencoded({extended:true}))
app.use("/api", routes)



app.listen(3000, () => {
    console.log("Server running on port 3000")
})
