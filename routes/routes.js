const express = require("express")
const router = express.Router()
const Model = require('../models/model')

//POST ROUTES
router.post("/post", async (req, res) => {
    const data = new Model({
        name: req.body.name,
        post: req.body.post,
        age: req.body.age
    })
    try {
        const dataToSave = await data.save();
        res.status(200).json(dataToSave)
    } catch (error) {
        res.status(400).json({message:error.message})
    }
})


// GET ALL POSTS
router.get("/getall", async (req, res) => {
    try {
        const data = await Model.find();
        res.json(data)
    } catch (error) {
        res.status(500).json({message:error.message})
    }
})


// GET BY ID POST
router.get("/getOne/:id", async (req, res) => {
    try {
        const data = await Model.findById(req.params.id);
        res.json(data)
    } catch (error) {
        res.status(500).json({message:error.message})
    }
})



// UPDATE BY ID
router.patch("/update/:id", async (req, res) => {        
    try {
        const id = req.params.id;
        const updateData = req.body;
        const options = {new: true};
        
        const result = await Model.findByIdAndUpdate(id, updateData, options)

        res.send(result)
    } catch (error) {
        res.status(400).json({message:error.message})
    }
})


// DELETE by ID
router.delete("/delete/:id", async (req, res) => {
    try {
        const id = req.params.id;
        const data = await Model.findByIdAndDelete(id)
        res.send(`Document with the ${id} & ${data.name} is being deleted successfully`)

    } catch (error) {
        res.status(400).json({message:error.message})
    }
})


module.exports = router